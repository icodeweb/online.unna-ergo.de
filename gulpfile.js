var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var nodemon = require('gulp-nodemon');
var livereload = require('gulp-livereload');
var minifyCss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var order = require('gulp-order');

var baseDirs = {
    app: './',
};

var publicDirs = {
    public: 'public/',
    js: 'public/javascripts/',
    css: 'public/stylesheets/',
    img: 'public/images/'
};

var bowerComponentsDir = baseDirs.app + 'public/bower_components/';
var appFiles = { js: [bowerComponentsDir + '**/*.min.js'], css: [bowerComponentsDir + '**/*.min.css'] };
var concatFilenames = { js: 'bower.js', css: 'bower.css' };


gulp.task('dev:concatjs', function () {
    return gulp.src([
        'public/bower_components/jquery/dist/jquery.min.js',
        'public/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'public/bower_components/tooltipster/dist/js/tooltipster.bundle.min.js',
        'public/bower_components/moment/min/moment.min.js',
        'public/bower_components/fullcalendar/dist/fullcalendar.min.js',
        'public/bower_components/fullcalendar/dist/locale/de.js',
        'public/bower_components/select2/dist/js/select2.full.min.js'
    ])
        .pipe(concat(concatFilenames.js))
        .pipe(gulp.dest(baseDirs.app + publicDirs.js));
});
gulp.task('dev:minifyjs', function() {
    return gulp.src(baseDirs.app + publicDirs.js + concatFilenames.js)
        .pipe(uglify())
        .pipe(gulp.dest(baseDirs.app + publicDirs.js));
});

gulp.task('dev:concatcss', function () {
    return gulp.src([
        'public/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'public/bower_components/animate.css/animate.min.css',
        'public/bower_components/tooltipster/dist/css/tooltipster.bundle.min.css',
        //'public/bower_components/fullcalendar/dist/fullcalendar.print.min.css',
        'public/bower_components/fullcalendar/dist/fullcalendar.min.css',
        'public/bower_components/select2/dist/css/select2.min.css'

    ])
        .pipe(concat(concatFilenames.css))
        .pipe(gulp.dest(baseDirs.app + publicDirs.css));
});
gulp.task('dev:minifycss', function() {
    return gulp.src(baseDirs.app + publicDirs.css + concatFilenames.css)
        .pipe(minifyCss())
        .pipe(gulp.dest(baseDirs.app + publicDirs.css));
});

gulp.task('nodemon', function () {
    nodemon({
        script: 'app.js'
    }).on('restart', function () {
        console.log('Magic restarted');
    }).on('crash', function() {
        console.error('Application has crashed!\n');
        stream.emit('restart', 1)  // restart the server in 10 seconds
    });
});

gulp.task('default', ['nodemon']);
gulp.task('cm', ['dev:concatjs', 'dev:minifyjs', 'dev:concatcss', 'dev:minifycss']);
