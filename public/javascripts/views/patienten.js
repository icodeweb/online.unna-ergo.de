$(function () {
    fillPatientenSelect();
    initBehandlungsformSelect();

    $(".patienten-select").on('change', function(){
        var patientId = $(this).val();
        selectPatient(patientId);
    });

    if($(".patienten-select").val() == 0){
        resetPatientInformation();
        $(".saveAccountChanges").html("Neuen Benutzer anlegen");
        $(".saveAccountChanges").unbind('click').bind('click', function(){
            addNewPatient();
        });
    }
});

function fillPatientenSelect(){
    $(".patienten-select").empty();
    $.ajax({
        url: '/patienten',
        dataType: 'json',
        method: 'get',
        async: false,
    }).done(function(data){
        var selectData = [{
            id: 0,
            text: "--- Neuen Patienten erstellen"
        }];
        $.each(data, function(key, val){
            selectData.push({id: val["id"], text: val["FULLNAME"]});
        });
        $(".patienten-select").select2({
            data: selectData,
            cache: false,
            language: "de",
            "language": {
                "noResults": function(data){
                    return "Keine Ergebnisse gefunden";
                }
            },
            minimumInputLength: -1
        });
    });
}

function initBehandlungsformSelect() {
    $(".behandlungsform-select").select2({
        cache: false,
        language: "de",
        "language": {
            "noResults": function(data){
                return "Keine Ergebnisse gefunden";
            }
        },
        minimumInputLength: -1
    });

}

function selectPatient(patientId){
    if(patientId != 0){
        $(".saveAccountChanges").html("Änderungen speichern");
        console.log(patientId);
        fillPatientInformation(patientId);
        $(".saveAccountChanges").unbind('click').bind('click', function () {
            const patientInformation = {};
            patientInformation.vorname = $(".accountInformation").find(".vorname").val();
            patientInformation.nachname = $(".accountInformation").find(".nachname").val();
            patientInformation.strasse = $(".accountInformation").find(".strasse").val();
            patientInformation.hausnummer = $(".accountInformation").find(".hausnummer").val();
            patientInformation.plz = $(".accountInformation").find(".plz").val();
            patientInformation.ort = $(".accountInformation").find(".ort").val();

            patientInformation.telefonnummer = $(".accountInformation").find(".telefonnummer").val();
            patientInformation.handynummer = $(".accountInformation").find(".handynummer").val();
            patientInformation.geburtsdatum = $(".accountInformation").find(".geburtsdatum").val();
            patientInformation.krankenkasse = $(".accountInformation").find(".krankenkasse").val();
            patientInformation.behandlungsform = $(".accountInformation").find(".behandlungsform-select").val();
            patientInformation.sonstigeHinweise = $(".accountInformation").find(".sonstigeHinweise").val();

            $.ajax({
                url: '/patienten/update',
                async: false,
                dataType: "json",
                method: 'POST',
                data: {
                    patientInformation: patientInformation,
                    patientId: patientId
                },
                success: function(response){
                    if(response["status"] == "true"){
                        alert("Änderungen wurden gespeichert")
                    }
                },
                error: function(error){
                    console.log(error);
                }
            });
        });
    } else {
        resetPatientInformation();
        $(".saveAccountChanges").html("Neuen Benutzer anlegen");
        $(".saveAccountChanges").unbind('click').bind('click', function(){
            addNewPatient();
        });
    }
}

function resetPatientInformation(){
    $(".accountInformation").find("input").each(function(){
        $(this).val("");
    });
}

function fillPatientInformation(patientId){
    $.ajax({
        url: '/patienten/' + patientId,
        type: 'get',
        async: false,
        dataType: "json",
        success: function(result) {
            console.log(result);
            var patienObject = result[0];
            $("input.vorname").val(patienObject["vorname"]);
            $("input.nachname").val(patienObject["nachname"]);
            $("input.strasse").val(patienObject["strasse"]);
            $("input.hausnummer").val(patienObject["hausnummer"]);
            $("input.plz").val(patienObject["plz"]);
            $("input.ort").val(patienObject["ort"]);

            $("input.telefonnummer").val(patienObject["telefonnummer"]);
            $("input.handynummer").val(patienObject["handynummer"]);
            $("input.geburtsdatum").val(patienObject["geburtsdatum"]);
            $("input.krankenkasse").val(patienObject["krankenkasse"]);
            $(".behandlungsform-select").val(patienObject["behandlungsform"]).trigger("change");
            $("input.sonstigeHinweise").val(patienObject["sonstigeHinweise"]);
        },
        error: function(error){
            console.log(error);
        }
    });
}

function addNewPatient() {
    var patientInformation = {};
    patientInformation.vorname = $(".accountInformation .vorname").val();
    patientInformation.nachname = $(".accountInformation .nachname").val();
    patientInformation.strasse = $(".accountInformation .strasse").val();
    patientInformation.hausnummer = $(".accountInformation .hausnummer").val();
    patientInformation.plz = $(".accountInformation .plz").val();
    patientInformation.ort = $(".accountInformation .ort").val();

    patientInformation.telefonnummer = $(".accountInformation .telefonnummer").val();
    patientInformation.handynummer = $(".accountInformation .handynummer").val();
    patientInformation.geburtsdatum = $(".accountInformation .geburtsdatum").val();
    patientInformation.krankenkasse = $(".accountInformation .krankenkasse").val();
    patientInformation.behandlungsform = $(".behandlungsform-select").val();
    patientInformation.sonstigeHinweise = $(".accountInformation .sonstigeHinweise").val();

    $.ajax({
        url: '/patienten/add',
        method: 'put',
        dataType: 'json',
        data: {
            patientInformation: patientInformation
        },
        async: false
    }).done(function(response){
        if(response != false){
            alert("Patient wurde angelegt");
            fillPatientenSelect();
            resetPatientInformation();
        }
    });
}