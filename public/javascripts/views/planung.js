$(function () {
    loadCalendar();
    $(".onoffswitch-checkbox").change(function () {
        refreshCalendar();
    });
    fillMitarbeiterModalSelect();
    fillPatientenModalSelect();
});

function getMitarbeiterIds() {
    var mitarbeiterIds = [];
    $(".onoffswitch-checkbox").each(function () {
        if($(this).is(":checked")) {
            mitarbeiterIds.push($(this).val());
        }
    })
    var mitarbeiterString = mitarbeiterIds.join(",");
    return mitarbeiterString;
}

function loadCalendar() {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,agendaDay,listWeek'
        },
        weekends: false, // Hide weekends
        defaultView: 'agendaWeek', // Only show week view
        navLinks: true,
        nowIndicator: true,
        slotEventOverlap: false,
        height: $(window).height()*0.69,
        minTime: '08:00:00', // Start time for the calendar
        maxTime: '22:00:00', // End time for the calendar
        slotDuration: '00:05:00',
        snapDuration: '00:05:00',
        displayEventTime: true,
        events: {
            url: '/planung',
            type: 'POST',
            data: {
                mitarbeiter: "1,2,3,7,8"
            },
            editable: true
        },
        dayClick: function(date, jsEvent, view)
        {
            $(".clickedTimeSlot").val(moment(date).unix());
            $('#calendarModal').modal();
            $(".saveTermin").unbind("click").bind("click", function () {
                var mitarbeiterId = $(".select-mitarbeiter-modal").val();
                var patientId = $(".select-patient-modal").val();
                var startTime = $(".clickedTimeSlot").val();
                addTermin(mitarbeiterId, patientId, startTime);
            });
        },
        eventDrop: function (event, delta, revertFunc) {
            var eventNewStart = moment(event.start.format()).format();
            var eventNewEnd = moment(event.end.format()).format();
            var eventId = event.id;

            $.ajax({
                url: '/planung/change',
                dataType: 'json',
                method: 'post',
                data: {
                    id: eventId,
                    start: eventNewStart,
                    end: eventNewEnd
                },
                async: false,
            }).done(function(){
                refreshCalendar();
            }).fail(function (error) {
                console.log(error);
            });
            console.log(eventId);
        },
        eventRender: function(event, element) {
            element.append( "<span class='closeon'>x</span>" );
            element.find(".closeon").click(function() {
                deleteTermin(event.id);
            });
        }
    });
}

function fillMitarbeiterModalSelect(){
    $(".select-mitarbeiter-modal").empty();
    $.ajax({
        url: '/mitarbeiter',
        dataType: 'json',
        method: 'get',
        async: false,
    }).done(function(data){
        var selectData = [{
            id: 0,
            text: "--- Mitarbeiter auswählen."
        }];
        $.each(data, function(key, val){
            selectData.push({id: val["id"], text: val["FULLNAME"]});
        });
        $(".select-mitarbeiter-modal").select2({
            data: selectData,
            cache: false,
            language: "de",
            "language": {
                "noResults": function(data){
                    return "Keine Ergebnisse gefunden";
                }
            },
            minimumInputLength: -1
        });
    });
}

function fillPatientenModalSelect(){
    $(".select-patient-modal").empty();
    $.ajax({
        url: '/patienten',
        dataType: 'json',
        method: 'get',
        async: false,
    }).done(function(data){
        var selectData = [{
            id: 0,
            text: "--- Patient auswählen."
        }];
        $.each(data, function(key, val){
            selectData.push({id: val["id"], text: val["FULLNAME"]});
        });
        $(".select-patient-modal").select2({
            data: selectData,
            cache: false,
            language: "de",
            "language": {
                "noResults": function(data){
                    return "Keine Ergebnisse gefunden";
                }
            },
            minimumInputLength: -1
        });
    });
}

function refreshCalendar() {
    mitarbeiterUrl = getMitarbeiterIds();
    var urlConfig = {
        url: '/planung',
        type: 'POST',
        data: {
            mitarbeiter: mitarbeiterUrl
        },
        editable: true
    };
    $('#calendar').fullCalendar('removeEventSource', urlConfig);
    $('#calendar').fullCalendar('addEventSource', urlConfig);
}

function addTermin(mitarbeiterId, patientId, startTime) {
    var behandlungszeit = $.xResponse("/planung/"+patientId);
    var startTime = startTime - 3600;
    var behandlungszeit = startTime  + (behandlungszeit.time * 60);
    $.ajax({
        url: '/planung',
        dataType: 'json',
        method: 'put',
        data: {
            mitarbeiterId: mitarbeiterId,
            patientId: patientId,
            start: moment.unix(startTime).format(),
            end: moment.unix(behandlungszeit).format()
        },
        async: false,
    }).done(function(){
        refreshCalendar();
    }).fail(function (error) {
        console.log(error);
    });
}
function deleteTermin(terminId) {
    $.ajax({
        url: '/planung',
        dataType: 'json',
        method: 'delete',
        data: {
            terminId: terminId
        },
        async: false,
    }).done(function(){
        refreshCalendar();
    }).fail(function (error) {
        console.log(error);
    });
}