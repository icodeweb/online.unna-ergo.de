$(function () {
    fillMitarbeiterSelect();

    $(".mitarbeiter-select").on('change', function(){
        var mitarbeiterId = $(this).val();
        selectMitarbeiter(mitarbeiterId);
    });

    if($(".mitarbeiter-select").val() == 0){
        resetMitarbeiterInformation();
        $(".saveAccountChanges").html("Neuen Mitarbeiter anlegen");
        $(".saveAccountChanges").unbind('click').bind('click', function(){
            addNewMitarbeiter();
        });
    }
});

function fillMitarbeiterSelect(){
    $(".mitarbeiter-select").empty();
    $.ajax({
        url: '/mitarbeiter',
        dataType: 'json',
        method: 'get',
        async: false,
    }).done(function(data){
        var selectData = [{
            id: 0,
            text: "--- Neuen Mitarbeiter erstellen"
        }];
        $.each(data, function(key, val){
            selectData.push({id: val["id"], text: val["FULLNAME"]});
        });
        $(".mitarbeiter-select").select2({
            data: selectData,
            cache: false,
            language: "de",
            "language": {
                "noResults": function(data){
                    return "Keine Ergebnisse gefunden";
                }
            },
            minimumInputLength: -1
        });
    });
}

function selectMitarbeiter(mitarbeiterId){
    if(mitarbeiterId != 0){
        $(".saveAccountChanges").html("Änderungen speichern");
        fillMitarbeiterInformation(mitarbeiterId);
        $(".saveAccountChanges").unbind('click').bind('click', function () {
            const mitarbeiterInformation = {};
            mitarbeiterInformation .vorname = $(".accountInformation").find(".vorname").val();
            mitarbeiterInformation .nachname = $(".accountInformation").find(".nachname").val();
            mitarbeiterInformation .strasse = $(".accountInformation").find(".strasse").val();
            mitarbeiterInformation .hausnummer = $(".accountInformation").find(".hausnummer").val();
            mitarbeiterInformation .plz = $(".accountInformation").find(".plz").val();
            mitarbeiterInformation .ort = $(".accountInformation").find(".ort").val();

            $.ajax({
                url: '/mitarbeiter/update',
                async: false,
                dataType: "json",
                method: 'POST',
                data: {
                    mitarbeiterInformation: mitarbeiterInformation,
                    mitarbeiterId: mitarbeiterId
                },
                success: function(response){
                    if(response["status"] == "true"){
                        alert("Änderungen wurden gespeichert")
                    }
                },
                error: function(error){
                    console.log(error);
                }
            });
        });
    } else {
        resetMitarbeiterInformation();
        $(".saveAccountChanges").html("Neuen Mitarbeiter anlegen");
        $(".saveAccountChanges").unbind('click').bind('click', function(){
            addNewMitarbeiter();
        });
    }
}

function resetMitarbeiterInformation(){
    $(".accountInformation").find("input").each(function(){
        $(this).val("");
    });
}

function fillMitarbeiterInformation(mitarbeiterId){
    $.ajax({
        url: '/mitarbeiter/' + mitarbeiterId,
        type: 'get',
        async: false,
        dataType: "json",
        success: function(result) {
            var mitarbeiterObject = result[0];
            $("input.vorname").val(mitarbeiterObject["vorname"]);
            $("input.nachname").val(mitarbeiterObject["nachname"]);
        },
        error: function(error){
            console.log(error);
        }
    });
}

function addNewMitarbeiter() {
    var mitarbeiterInformation = {};
    mitarbeiterInformation.vorname = $(".accountInformation .vorname").val();
    mitarbeiterInformation.nachname = $(".accountInformation .nachname").val();

    $.ajax({
        url: '/mitarbeiter/add',
        method: 'put',
        dataType: 'json',
        data: {
            mitarbeiterInformation: mitarbeiterInformation
        },
        async: false
    }).done(function(response){
        if(response != false){
            alert("Mitarbeiter wurde angelegt")
        }
    });
}