$(function () {
    var weeknumber = moment().isoWeek();
    $(".panel-weeknumber > .panel-bottom span").html("KW-" + weeknumber);

    $.ajax({
        url: '/dashboard',
        dataType: 'json',
        method: 'get',
        async: false,
    }).done(function(result) {
        $(".panel-patienten > .panel-bottom span").html(result["patienten"]);
        $(".panel-mitarbeiter > .panel-bottom span").html(result["mitarbeiter"]);

        $(".panel-patienten").on('click', function(){
            window.location.hash = 'patienten';
        });
        $(".panel-mitarbeiter").on('click', function(){
            window.location.hash = 'mitarbeiter';
        });
        $(".panel-weeknumber").on('click', function(){
            window.location.hash = 'planung';
        });
    });
});