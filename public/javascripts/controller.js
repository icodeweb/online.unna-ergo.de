$(window).on("load", function () {
    hash = decodeURI(window.location.hash);
    hash = hash.split("?");
    hash = hash[0];
    hash = hash.substring(1, hash.length);
    render(hash);
});

$(window).on('hashchange', function () {
    hash = decodeURI(window.location.hash);
    hash = hash.split("?");
    hash = hash[0];
    hash = hash.substring(1, hash.length);
    render(hash);
});

function render(hash) {
    if(hash.length < 1){
        hash = "dashboard";
    }
    $.ajax( {
        url: "views/" + hash,
    }).done(function (html) {
        $(".main-content").html(html).promise().done(function(){
            $(".page").addClass("fadeInDown").fadeIn(300);
        });
    });
}