function debug(what, name){
    console.log("\n- - - - - - - - - - - - - - - - - - -\n");
    console.log(name);
    console.log(what);
}
$.extend({
    xResponse: function(url) {
        var theResponse = null;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: "json",
            async: false,
            success: function(respText) {
                theResponse = respText;
            }
        });
        return theResponse;
    }
});
function getGoogleInformation(destination) {
    var distance = "";
    var duration = "";
    $.ajax({
        url: "https://maps.googleapis.com/maps/api/distancematrix/json",
        type: "GET",
        data: {
            "language": "de-DE",
            "key": "AIzaSyCOVJxZoBIRbjeIRyQOQrhEEt2riP1BkyA",
            "origins": "Märkische+Straße+9a+59423+Unna+Deutschland",
            "destinations": destination,
        },
    }).done(function(data){
        if(data["status"] == "OK") {
            distance = data["rows"]["elements"]["distance"]["value"];
            duration = data["rows"]["elements"]["duration"]["value"];
        }
    });
    return {
        distance: distance,
        duration: duration
    }
}