const express         = require('express');
const http            = require('http');
const path            = require('path');
const app             = express();
const bodyParser      = require('body-parser');
const mysql           = require('mysql2');

// configuration settings for PORT
app.set('port', process.env.PORT || 8081);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/public'));
app.set('views', path.join(__dirname, '/views')); // Convenience since it's the default anyway.
app.set('view engine', 'pug');

// database ================================================
const db = require('./config/db');
const connection = mysql.createPool({
    connectionLimit     : 100,
    host                : db.host,
    user                : db.user,
    password            : db.password,
    database            : db.database,
    debug               : false
});

app.use(function(req,res,next){
    req.db = connection;
    next();
});

// routes ==================================================
require('./routes/routes')(app);
require('./routes/dashboard')(app);
require('./routes/mitarbeiter')(app);
require('./routes/patienten')(app);
require('./routes/planung')(app);


const server = http.createServer(app).listen(app.get('port'), function() {
    console.log("Magic happens on port ==> " + app.get('port'));
});
exports = module.exports = app;