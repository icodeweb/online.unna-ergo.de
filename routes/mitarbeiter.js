module.exports = function(app) {
    app.get('/mitarbeiter', function (req, res) {
        var db = req.db;
        var sql = 'SELECT CONCAT(m.vorname, " ", m.nachname ) AS FULLNAME, id FROM MITARBEITER m ORDER BY FULLNAME ASC;';
        db.query(sql, function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                console.log(err);
            }
        });
    });

    app.get('/mitarbeiter/:mitarbeiterId', function(req, res){
        var db = req.db;
        var mitarbeiterId = req.params.mitarbeiterId;
        var sql = "SELECT * FROM MITARBEITER WHERE id = ?;";
        db.query(sql, [mitarbeiterId], function (err, rows) {
            if(!err) {
                res.json(rows)
            } else {
                console.log(err);
            }
        });
    });

    app.put('/mitarbeiter/add', function(req, res){
        var db = req.db;
        var info = req.body.mitarbeiterInformation;

        var sql = "INSERT INTO MITARBEITER (vorname,nachname) VALUES ('"+info.vorname+"', '"+info.nachname+"');"
        db.query(sql, function (err, rows) {
            if(!err) {
                res.json(rows.insertId);
            } else {
                res.json(false);
            }
        });
    });

    app.post('/mitarbeiter/update', function(req, res){
        var db = req.db;
        var info = req.body.mitarbeiterInformation;
        var mitarbeiterId = req.body.mitarbeiterId;
        var sql = "UPDATE MITARBEITER SET vorname='"+info.vorname+"', nachname='"+info.nachname+"' WHERE id=?;";
        db.query(sql, [mitarbeiterId], function (err) {
            if(!err) {
                res.json({"status": "true"});
            } else {
                res.json({"status": "false"});
            }
        });
    });
}