module.exports = function(app) {
    var async = require('async');

    app.get('/dashboard', function (req, res) {
        var db = req.db;

        async.parallel({
            patienten: function (callback) {
                var sql = "SELECT COUNT(*) AS count FROM PATIENT";
                db.query(sql, function (err, rows) {
                    if (!err) {
                        callback(null, rows[0].count)
                    } else {
                        console.log(err);
                    }
                });
            },
            mitarbeiter: function (callback) {
                var sql = "SELECT COUNT(*) AS count FROM MITARBEITER";
                db.query(sql, function (err, rows) {
                    if (!err) {
                        callback(null, rows[0].count)
                    } else {
                        console.log(err);
                    }
                });
            }
        }, function (err, results) {
            res.json(results);
        });
    });
}