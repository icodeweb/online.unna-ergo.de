module.exports = function(app) {
    // Hole alle Termine
    app.post('/planung', function (req, res) {
        var db = req.db;
        var mitarbeiterIds = req.body.mitarbeiter;
        var sql = 'SELECT IF (CONCAT(p.vorname, " ", p.nachname) IS NULL, t.alt, CONCAT(p.vorname, " ", p.nachname)) as title, t.start, t.end, t.id, m.backgroundColor FROM TERMINE t LEFT JOIN PATIENT p ON p.id = t.patient_id INNER JOIN MITARBEITER m ON m.id = t.mitarbeiter_id WHERE mitarbeiter_id IN ('+ mitarbeiterIds +');';
        db.query(sql, function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                console.log(err);
            }
        });
    });
    // Erstelle neuen Termin
    app.put('/planung', function (req, res) {
        var db = req.db;
        var mitarbeiter_id = req.body.mitarbeiterId;
        var patient_id = req.body.patientId;
        var start = req.body.start;
        var end = req.body.end;
        var sql = 'INSERT INTO TERMINE (mitarbeiter_id,patient_id,start,end) VALUES ('+mitarbeiter_id+','+patient_id+',"'+start+'","'+end+'")';
        console.log(sql);
        db.query(sql, function (err, rows) {
            if (!err) {
                res.json(true);
            } else {
                res.json(false);
            }
        });
    });
    // Lösche Termin
    app.delete('/planung', function (req, res) {
        var db = req.db;
        var terminId = req.body.terminId;
        var sql = 'DELETE FROM TERMINE WHERE id = ?';
        db.query(sql, [terminId], function (err, rows) {
            if (!err) {
                res.json(true);
            } else {
                res.json(false);
            }
        });
    });
    // Hole Behandlungszeit
    app.get('/planung/:patientId', function (req, res) {
        var db = req.db;
        var patientId = req.params.patientId;
        var sql = 'SELECT behandlungsform FROM PATIENT WHERE id = ?;';
        db.query(sql, [patientId], function (err, rows) {
            if (!err) {
                var time = 0;
                switch (rows[0].behandlungsform) {
                    case "Motorisch-Funktionell":
                        time = 30;
                        break;
                    case "Psychisch-Funktionell":
                        time = 60;
                        break;
                    case "Sensomotorisch-Perzeptiv":
                        time = 45;
                        break;
                    case "Hausbesuch":
                        time = 10;
                        break;
                    case "Hirnleistungstraining":
                        time = 35;
                        break;
                    case "Thermische Anwendung":
                        time = 15;
                        break;
                    case "Gruppe":
                        time = 60;
                        break;
                }
                res.json({time: time});
            } else {
                console.log(err);
            }
        });
    });
    //Update Termin nach verschieben
    app.post('/planung/change', function (req, res) {
        var db = req.db;
        var id = req.body.id;
        var start = req.body.start;
        var end = req.body.end;
        var sql = 'UPDATE TERMINE SET start = "'+start+'", end = "'+end+'" WHERE id = '+id+';';
        console.log(sql);
        db.query(sql, [id, start, end], function (err, rows) {
            if (!err) {
                res.json(true);
            } else {
                res.json(false);
            }
        });
    });
}