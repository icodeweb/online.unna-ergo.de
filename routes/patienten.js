module.exports = function(app) {
    app.get('/patienten', function (req, res) {
        var db = req.db;
        var sql = 'SELECT CONCAT(p.vorname, " ", p.nachname ) AS FULLNAME, id FROM PATIENT p ORDER BY FULLNAME ASC;';
        db.query(sql, function (err, rows) {
            if (!err) {
                res.json(rows);
            } else {
                console.log(err);
            }
        });
    });

    app.get('/patienten/:patientId', function(req, res){
        var db = req.db;
        var patientId = req.params.patientId;
        var sql = "SELECT * FROM PATIENT WHERE id = ?;";
        db.query(sql, [patientId], function (err, rows) {
            if(!err) {
                res.json(rows)
            } else {
                console.log(err);
            }
        });
    });

    app.put('/patienten/add', function(req, res){
        var db = req.db;
        var info = req.body.patientInformation;

        var sql = "INSERT INTO PATIENT (vorname,nachname,strasse,hausnummer,plz,ort,telefonnummer,handynummer,geburtsdatum,krankenkasse,sonstigeHinweise,behandlungsform) VALUES ('"+info.vorname+"', '"+info.nachname+"', '"+info.strasse+"', '"+info.hausnummer+"', '"+info.plz+"', '"+info.ort+"', '"+info.telefonnummer+"', '"+info.handynummer+"', '"+info.geburtsdatum+"', '"+info.krankenkasse+"', '"+info.sonstigeHinweise+"', '"+info.behandlungsform+"');";
        db.query(sql, function (err, rows) {
            if(!err) {
                res.json(rows.insertId);
            } else {
                res.json(false);
            }
        });
    });

    app.post('/patienten/update', function(req, res){
        var db = req.db;
        var info = req.body.patientInformation;
        var patientId = req.body.patientId;
        var sql = "UPDATE PATIENT SET vorname='"+info.vorname+"', nachname='"+info.nachname+"', strasse='"+info.strasse+"', hausnummer='"+info.hausnummer+"', plz='"+info.plz+"', ort='"+info.ort+"', telefonnummer='"+info.telefonnummer+"', handynummer='"+info.handynummer+"',geburtsdatum='"+info.geburtsdatum+"', krankenkasse='"+info.krankenkasse+"', sonstigeHinweise='"+info.sonstigeHinweise+"', behandlungsform='"+info.behandlungsform+"'  WHERE id=?;";
        db.query(sql, [patientId], function (err) {
            if(!err) {
                res.json({"status": "true"});
            } else {
                res.json({"status": "false"});
            }
        });
    });
}